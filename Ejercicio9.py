import math

print ("Bienvenido a la calculadora de dos puntos en el plano cartesiano")

#Se solicitan las coordenadas
x1 = int(input("Ingrese la coordena X del primer punto:"))
y1 = int(input("Ingrese la coordena Y del primer punto:"))

x2 = int(input("Ingrese la coordena X del segundo punto:"))
y2 = int(input("Ingrese la coordena Y del segundo punto:"))

#Se comprueban cual es la mayor para tener un numero positivo
if x1 < x2:
    distanciax = x2 - x1
elif x2 < x1:
    distanciax = x1 - x2

if y1 < y2:
    distanciay = y2 - y1
elif y2 < y1:
    distanciay = y1 - y2

#Se calcula la distancias y se imprime
distancia = math.sqrt (distanciay * distanciay + distanciax * distanciax)
print("La distancia de esos dos puntos seria de:", distancia)