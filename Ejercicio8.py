import math
#Se solicita los datos que se tienen de la figura
R = float(input("Ingrese el dato del cateto R:"))
H = float(input("Ingrese el dato de la hipotenusa H:"))

#Se realizan los calculos 
R2 = H*H - R*R
R2 = math.sqrt(R2) 

AREA1 = R * R2 / 2
#pi se valorizara como 3.14
AREA2 = 3.14 * R*R / 2
AREAFINAL = AREA1 * 2 + AREA2

#Se imprimen todas las areas y el total
print("El area de los triangulos serian de", AREA1, "c/u")
print("El area del semi circulo seria de", AREA2)
print("El area de la figura seria", AREAFINAL)